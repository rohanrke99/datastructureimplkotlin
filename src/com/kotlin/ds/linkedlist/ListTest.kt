package com.kotlin.ds.linkedlist


var utils = ListUtils<Int>()

fun main(args: Array<String>) {


    testReverseList()


}

fun testInsertInList() {

    val arr = arrayOf(10, 15, 12, 13, 20, 14)
    var list = utils.createLinkedList(arr)
    list?.printList()
}

fun testDeletionInList() {

    val arr = arrayOf(10, 15, 12, 13, 20, 14)
    var list = utils.createLinkedList(arr)
    list?.printList()
    println()
    list?.deleteNodeFromLinkedList(3)
    println()
    list?.printList()
}

fun testLengthInList() {

    val arr = emptyArray<Int>()
   // val arr = arrayOf(10, 15, 12, 13, 20, 14)
    var list = utils.createLinkedList(arr)
    list?.printList()
    println("length is " + list?.length())
}

fun testReverseList() {

    val arr = arrayOf(10, 20, 30, 40, 50, 60, 70, 80, 90, 100)
    val list  = utils.createLinkedList(arr)
    list?.printList()
    list?.reverseList()
    list?.printList()


}






