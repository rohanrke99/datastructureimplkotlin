package com.kotlin.ds.linkedlist



class LinkedList<T>{


    var headNode : ListNode<T>? = null

    fun insertInLinkedList(data:T,position: Int){

        var k = 1
        var p : ListNode<T>?
        var q : ListNode<T>? = null
        var newNode = ListNode<T>(data,null)
        p = headNode
        if (position == 1){
            newNode.next = p
            headNode = newNode

        }else {
            // Traverse the list until the position we want to insert
            while (p!=null && k<position){
                k++
                q = p
                p = p.next
            }
            q?.next = newNode
            newNode.next = p

        }

    }

    /**
     * Print Linked List
     *
     * @param head head node of the list
     */
    fun printList() {
        println()
        if (headNode == null) {
            return
        }
        var curr = headNode
        print("null")
        print(" -> ")
        while (curr != null) {
            print(curr.data)
            print(" -> ")
            curr = curr.next
        }
        print("null")
        println()
    }

    /**
     *
     * Deletion in a Linked List
     *
     * Time Complexity O(n)
     * Space Complexity O(1)
     *
     * @param head   -- head node of list
     * @param position --- position of insertion
     */
    fun deleteNodeFromLinkedList(position: Int){
        var head = headNode
        var k = 1
        var p: ListNode<T>?
        var q: ListNode<T>? = null

        if (head == null) {
            print("List is empty")
            return
        }
        p = head

        /* from the beginning */
        if (position == 1) {
            head = head.next
            headNode = head
            return
        } else {
            // Traverse the list until the position we want to delete
            while (p != null && k < position) {
                k++
                q = p
                p = p.next
            }
            if (p == null) {  /* At the End */
                print("Position doesn't exits")
            } else {    /* From  the middle */
                q!!.next = p.next
                p = null
            }
        }


    }


    fun length() : Int{
        var count = 0
        var node = headNode
        while (node!=null){
           ++ count
            node = node.next
        }

        return count
    }

    /**
     * Reverse a singly linked list
     *
     * Time Complexity O(n)
     * Space Complexity O(1)
     *
     *
     * @param head -- head of the list
     * @return
     */
    fun reverseList() {

        var prev: ListNode<T>? = null
        var curr: ListNode<T>?
        var next: ListNode<T>?
        curr = headNode
        while (curr != null) {
            next = curr.next
            curr.next = prev
            prev = curr
            curr = next
        }
        headNode = prev

    }


}