package com.kotlin.ds.linkedlist

data class ListNode<T>(var data : T, var next : ListNode<T>?)