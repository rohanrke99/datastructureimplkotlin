package com.kotlin.ds.linkedlist

import java.util.*

class ListUtils<T> {




    /**
     *
     * Create a Linked List
     *
     * Time Complexity O(n)
     * Space Complexity O(1)
     *
     * @param arr -- array from which list is to created
     */
    fun createLinkedList(arr: Array<Int>): LinkedList<Int>? {

        var list = LinkedList<Int>()
        var head: ListNode<Int>? = null
        var curr: ListNode<Int>? = null
        for (i in arr.indices) {
            var node = ListNode(arr[i], null)
            if (i == 0) {
                head = node
                curr = node
            }

            curr!!.next = node
            curr = curr.next
        }

        list.headNode = head

        return list
    }


}